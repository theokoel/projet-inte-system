using System.Collections.Generic;

namespace Projet_Inte_System.Api
{
    /// <summary>
    /// Class <c>Assets</c> List of Object create with Json from LunarCrush API with parameter data=assets
    /// </summary>
    public class Assets
    {
        public List<Data> Data { get; set; }
        
    }

    /// <summary>
    /// Class <c>Data</c> Object from LunarCrush which contains all data 
    /// </summary>
    public class Data
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }
        public string Price { get; set; }
        public string Price_btc { get; set; }
        public List<TimeSeries> TimeSeries { get; set; }

    }

    /// <summary>
    /// Class <c>TimeSeries</c> Data during a specific time.
    /// </summary>
    public class TimeSeries
    {
        public string Time { get; set; }
        public string Open { get; set; }
        public string Close { get; set; }
        public double PercentChange { get; set; }

        public string High { get; set; }
        public string Low { get; set; }
        public double Price_btc { get; set; }

        public double PercentChange_btc { get; set; }

        public string Volume { get; set; }  
    }
}
    