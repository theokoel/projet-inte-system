﻿using Projet_Inte_System.Api;
using Projet_Inte_System.Api.DataType;
using System;
using System.Collections.Generic;

namespace Projet_Inte_System.Util
{
    public class Util
    {
        public static DateTime UnixTimeToDate(string tps)
        {
            long temps = Convert.ToInt64(tps);
            System.DateTime dt = new(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dt = dt.AddSeconds(temps).ToLocalTime();
            return dt;
        }
    
        public static TimeSeriesTyped[] ConvertToTimeSeriesTyped(string synbol, string name, List<TimeSeries> timeSeries)
        {
            List<TimeSeriesTyped> toTimeSeriesTypes = new();

            for (int i = 0; i < timeSeries.Count-1; i++)
            {
                TimeSeriesTyped t = new(synbol, name , timeSeries[i].Time, timeSeries[i].Open, timeSeries[i].Close, timeSeries[i].High, timeSeries[i].Low, timeSeries[i].Price_btc, timeSeries[i].Volume);
                toTimeSeriesTypes.Add(t);
            }
            return toTimeSeriesTypes.ToArray();
        }

        public static TimeSeriesTyped[] ConvertToTimeSeriesTyped(Assets assets)
        {
            List<TimeSeries> timeSeries = assets.Data[0].TimeSeries;
            List<TimeSeriesTyped> toTimeSeriesTypes = new();

            for (int i = 0; i < timeSeries.Count-1; i++)
            {
                string volume = timeSeries[i].Volume == null ? "0" : timeSeries[i].Volume;
                TimeSeriesTyped t = new(assets.Data[0].Symbol,assets.Data[0].Name,timeSeries[i].Time, timeSeries[i].Open, timeSeries[i].Close, timeSeries[i].High, timeSeries[i].Low, timeSeries[i].Price_btc, volume);
                toTimeSeriesTypes.Add(t);
            }
            return toTimeSeriesTypes.ToArray();

        }
    }
    
}