using Projet_Inte_System.Api.DataType;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Projet_Inte_System.Util.SplitWork
{
    public class TimeSeriesOperations
    {
        private static List<double> PriceToPercent(List<double> priceList)
        {
            List<double> percentList = new List<double> {100};
            for(int i = 1 ;  i < priceList.Count ; i++)
            {
                double pct = 100 + (100 * (priceList[i] - priceList[i - 1]) / priceList[i]);
                percentList.Add(pct);
            }
            return percentList;
        }

        public static TimeSeriesTyped[] CalculateData(TimeSeriesTyped[] timeSeriesArray)
        {
            List<double> priceList = new List<double>();
            foreach (TimeSeriesTyped timeSeriesTypedItem in timeSeriesArray)
            {
                priceList.Add(timeSeriesTypedItem.Close);
            }
            List<double> priceListBtc = new List<double>();
            foreach (TimeSeriesTyped timeSeriesTypedItem in timeSeriesArray)
            {
                priceListBtc.Add(timeSeriesTypedItem.Price_btc);
            }

            List<double> percentList = PriceToPercent(priceList);
            List<double> percentListBtc = PriceToPercent(priceListBtc);


            for(int i = 0 ;  i < timeSeriesArray.Length ; i++)
            {
                timeSeriesArray[i].PercentChange = percentList[i];
                timeSeriesArray[i].PercentChange_btc = percentListBtc[i];
            }
            return timeSeriesArray;
        }

        private static double SquaredDeviationSum(List<double> priceList)
        {
            double avg = priceList.Average();
            return priceList.Sum(x => Math.Pow(x - avg, 2));
        } 

        public static double StandardDeviation(List<double> priceList)
        {
            double result = 0;
            if (priceList.Any())
            {
                double avg = priceList.Average();
                double sum = SquaredDeviationSum(priceList);
                result = Math.Sqrt((sum) / (priceList.Count() - 1));
            }
            return result;
        }

        public static double StandardDeviationPercent(List<double> priceList)
        {
            double SDev = StandardDeviation(priceList);
            return 100*(SDev/priceList[priceList.Count-1]);
        }

        public static double Correlation(List<double> priceList1, List<double> priceList2)
        {
            if(priceList1.Count != priceList2.Count)
            {
                throw new ArgumentException("values must be the same length");
            }
            double avg1 = priceList1.Average();
            double avg2 = priceList2.Average();
            double sum1 = priceList1.Zip(priceList2, (x1, y1) => (x1 - avg1) * (y1 - avg2)).Sum();
            double result = sum1 / Math.Sqrt(SquaredDeviationSum(priceList1) * SquaredDeviationSum(priceList2));
            return result;
        }

        public static double Beta(List<double> priceList, List<double> benchmark)
        {
            return Correlation(priceList, benchmark) * StandardDeviation(priceList) / StandardDeviation(benchmark);
        }

        public static List<double> TimeSeriesToPriceList(TimeSeriesTyped[] timeSeriesArray)
        {
            List<double> priceList = new List<double>();
            foreach (TimeSeriesTyped timeSeriesTypedItem in timeSeriesArray)
            {
                priceList.Add(timeSeriesTypedItem.Close);
            }
            return priceList;
        }

    }
}