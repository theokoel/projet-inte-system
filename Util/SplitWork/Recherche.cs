using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Projet_Inte_System.Util.SplitWork
{
    public class Recherche
    {

        static string inputSearsh = "";
        static List<DataRearsh> listeCrypto = new List<DataRearsh>();

        public static void setInputSearch(string input)
        {
            inputSearsh = input;
        }

        public static List<DataRearsh> getResultSearch()
        {
            List<DataRearsh> filtredList = new List<DataRearsh>();
            if (listeCrypto.Count > 0)
            {
                foreach (DataRearsh dataRearsh in listeCrypto)
                {
                    if (inputSearsh != "" && inputSearsh.Length > 0 && (dataRearsh.Name.ToLower().Contains(inputSearsh.ToLower()) || dataRearsh.Symbol.ToLower().Contains(inputSearsh.ToLower())))
                    {
                        filtredList.Add(dataRearsh);
                    }
                }
            }
            return filtredList;
        }

        public static void setListeCrypto(dynamic liste)
        {
            AssetsRearsh AssetsRearsh = JsonToAssets(Convert.ToString(liste));;
            listeCrypto = AssetsRearsh.Data;
        }

        public static AssetsRearsh JsonToAssets(string json)
        {
            AssetsRearsh assetItem = JsonConvert.DeserializeObject<AssetsRearsh>(json);
            return assetItem;
        }

    }


    public class AssetsRearsh
    {
        public List<DataRearsh> Data { get; set; }
    }

    public class DataRearsh
    {
        public string Name { get; set; }
        public string Symbol { get; set; }

    }
}