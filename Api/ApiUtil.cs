using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Projet_Inte_System.Util.SplitWork;

namespace Projet_Inte_System.Api
{
    
    /// <summary>
    /// Class <c>ApiUtil</c> All functionalities about using API. 
    /// </summary>
    public class ApiUtil 
    {
        //public static string APIKey = "ucp9mcro6hu179j97jyyb";
        public static string APIKey = "y248znymw15m7an1036oe";

        public static string GetApi(string dataType, string symbol) 
        {
            string url = "https://api.lunarcrush.com/v2?key=" + APIKey + "&data=" + dataType + "&symbol=" + symbol;
            return url;
        }

        public static string GetApi(string dataType, string symbol, string type) 
        {
            string url = "https://api.lunarcrush.com/v2?key=" + APIKey + "&data=" + dataType + "&symbol=" + symbol + "&type=" + type;
            return url;
        }

        public static string GetApiFeed(string dataType, string symbol, string sources)
        {
            string url = "https://api.lunarcrush.com/v2?key=" + APIKey + "&data=" + dataType + "&symbol=" + symbol + "&sources=" + sources ;
            return url;
        }

        public static Assets JsonToAssets(string json)
        {
            Assets assetItem = JsonConvert.DeserializeObject<Assets>(json);
            return assetItem;
        }

        public static TwitterData JsonToTwitterData(string json)
        {
            TwitterData twitterData = JsonConvert.DeserializeObject<TwitterData>(json);
            return twitterData;
        }
        
        public static DescriptionData JsonToDescrptionData(string json)
        {
            DescriptionData descripionData = JsonConvert.DeserializeObject<DescriptionData>(json);
            return descripionData;
        }
    }
}