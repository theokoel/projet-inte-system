﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projet_Inte_System.Util;

namespace Projet_Inte_System.Api.DataType
{
    public class TimeSeriesTyped
    {
        public string Symbol { get; set; }
        public string Name { get; set; }
        public string SymbolName { get; set; }
        public DateTime Time { get; set; }
        public double Open { get; set; }
        public double Close { get; set; }

        public double PercentChange { get; set; }

        public double High { get; set; }
        public double Low { get; set; }
        public double Price_btc { get; set; }
        public double PercentChange_btc { get; set; }

        public string Volume { get; set; }

        public TimeSeriesTyped(string symbol, string name, string time, string open, string close, string high, string low, double price_btc, string volume)
        {
            Symbol = symbol;
            Name = name;
            SymbolName = Symbol + " - " + Name;
            Time = Util.Util.UnixTimeToDate(time);
            Open = Convert.ToDouble(open);
            Close = Convert.ToDouble(close);
            High = Convert.ToDouble(high);
            Low = Convert.ToDouble(low);
            Price_btc = Convert.ToDouble(price_btc);
            Volume = volume;
        }

        public TimeSeriesTyped(string symbol, string name, TimeSeries timesSerie)
        {
            Symbol = symbol;
            Name = name;
            SymbolName = Symbol + " - " + Name;
            Time = Util.Util.UnixTimeToDate(timesSerie.Time);
            Open = Convert.ToDouble(timesSerie.Open);
            Close = Convert.ToDouble(timesSerie.Close);
            High = Convert.ToDouble(timesSerie.High);
            Low = Convert.ToDouble(timesSerie.Low);
            Price_btc = Convert.ToDouble(timesSerie.Price_btc);
            Volume = timesSerie.Volume;
        }
    }
}