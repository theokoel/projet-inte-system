using System.Collections.Generic;

namespace Projet_Inte_System.Util.SplitWork
{
    public class DescriptionCrypto
    {

    }

    public class TwitterData
    {
        public List<TwitterInformation> Data { get; set; }
    }
    public class TwitterInformation
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string Display_name { get; set; }
        public string Profile_image { get; set; }
        public string Twitter_screen_name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Publisher { get; set; }
        public string Url { get; set; }
    }

    public class DescriptionData
    {
        public List<MetaInformation> Data { get; set; }
    }

    public class MetaInformation
    {
        public string Name { get; set; }
        public string Symbol { get; set; }
        public string Description { get; set; }
        public string Image { get; set;}
    }
}